#!/bin/bash
sed '/FFNBeagle/d' /etc/hosts | sudo tee /tmp/stripped-etc-hosts
cat /tmp/stripped-etc-hosts | sudo tee /etc/hosts

echo 192.168.1.13 FFNBeagle-Box-1 | sudo tee -a /etc/hosts
echo 192.168.1.14 FFNBeagle-Box-3 | sudo tee -a /etc/hosts
echo 192.168.1.36 FFNBeagle-A-1   | sudo tee -a /etc/hosts
echo 192.168.1.12 FFNBeagle-A-3   | sudo tee -a /etc/hosts

ssh-copy-id  -f debian@ffnbeagle-box-1
ssh-copy-id  -f debian@ffnbeagle-box-3
ssh-copy-id  -f debian@ffnbeagle-A-1    
ssh-copy-id  -f debian@ffnbeagle-A-3    
exit 0
