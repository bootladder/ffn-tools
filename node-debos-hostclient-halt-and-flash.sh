#!/bin/bash
if [ $# -ne 2 ]; then
	echo need 2 arguments, target host and firmware image
	exit 1
fi
scp $2 debian@$1:/tmp/tempflashimage
ssh debian@$1 "cd /opt/ffn/bin; ./debos-hostclient.out -d /dev/ttyUSB0 -H; ./debos-hostclient.out -d /dev/ttyUSB0 -w /tmp/tempflashimage"
	
